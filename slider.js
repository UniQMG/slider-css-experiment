let input = document.querySelector('.sideium input');

const left = defineResource('leftium');
const right = defineResource('rightium');

defineResource('sliderstate')
  //.bind(val => slider.style.setProperty('--state', val))
  .calculate(() => {
    let leftium = left.value;
    let rightium = right.value;
    let total = leftium + rightium;
    if (total == 0) return 0.5;
    return leftium / total;
  })
  .listen(left, right);

left.derive('left-pressure', (val) => {
  return val / (resources.sliderstate.value);
}).derive('left-pressure-high', (val) => {
  return (val > 1000) * 1;
});

right.derive('right-pressure', (val) => {
  return val / (1-resources.sliderstate.value);
}).derive('right-pressure-high', (val) => {
  return (val > 1000) * 1;
});

let last = input.value;
input.onmousemove = function() {
  let delta = input.value - last;
  if (delta == 0) return;
  last = input.value;

  left.value += -Math.min(delta, 0);
  right.value += Math.max(delta, 0);
};
