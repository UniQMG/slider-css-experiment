defineResource('bodyelements').bind(val => {
  document.querySelector('.bodyclasses .elements').innerHTML = val;
}).calculate(() => {
  return [...document.body.classList].map(e => `<li>${e}</li>`).join('');
}).listen(resources.leftium, resources.rightium);

defineResource('bodyvariables').bind(val => {
  document.querySelector('.bodyvariables .elements').innerHTML = val;
}).calculate(() => {
  return [...document.body.style].map(styleName => {
    if (styleName == '--resource-bodyelements')
      return [styleName, '[not shown because of recursive reference]'];
    if (styleName == '--resource-bodyvariables')
      return [styleName, '[not shown because of recursive reference]'];
    return [styleName, document.body.style.getPropertyValue(styleName)];
  }).map(([n, e]) => `<li>${n}: ${e}</li>`).join('');
}).listen(resources.leftium, resources.rightium);
