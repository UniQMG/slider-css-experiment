const resources = {};

class Resource {
  constructor(name) {
    this.name = name;
    this._value = 0;
    this.valueAtLastChange = 0;
    this.onChangeHooks = [];
    this.calculateHooks = [];

    this.bind(val => {
      document.body.style.setProperty('--resource-' + this.name, val);
      document.body.classList.toggle('resource-' + this.name, !!val);
    });
    setTimeout(() => this.recalculate());
  }

  set value(val) {
    this._value = val;
    this.fireChangeHooks();
  }

  get value() {
    this.fireCalculateHooks();
    return this._value;
  }

  fireChangeHooks() {
    let delta = this.valueAtLastChange - this._value;
    this.valueAtLastChange = this._value;
    for (let callback of this.onChangeHooks) {
      callback(this._value, delta);
    }
  }

  fireCalculateHooks() {
    for (let callback of this.calculateHooks) {
      this._value = callback(this._value);
    }
  }

  /**
   * @param {Function<Number value, Number delta>} callback
   * @return {Resource} this
   */
  bind(callback) {
    this.onChangeHooks.push(callback);
    return this;
  }

  /**
   * @param {Number Function<Number currentValue>} callback
   * @return {Resource} this
   */
  calculate(callback) {
    this.calculateHooks.push(callback);
    return this
  }

  /**
   * @param {...Resource} resources
   * @return {Resource} this
   */
  listen(...resources) {
    for (let res of resources) {
      res.bind(() => this.recalculate());
    }
    return this;
  }

  recalculate() {
    this.fireCalculateHooks();
    this.fireChangeHooks();
  }

  /**
   * @param {String} name name of the new resource
   * @param {Number Function<Number thisValue>} callback
   *   calculate hook based off of parent value
   */
  derive(name, callback) {
    let resource = defineResource(name);
    resource.calculate(() => callback(this.value));
    this.bind(() => resource.recalculate());
    return resource;
  }
}

function defineResource(name) {
  let resource = new Resource(name);
  resources[name] = resource;
  return resource;
}
